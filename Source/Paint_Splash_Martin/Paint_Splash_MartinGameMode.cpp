// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Paint_Splash_MartinGameMode.h"
#include "Paint_Splash_MartinHUD.h"
#include "Paint_Splash_MartinCharacter.h"
#include "UObject/ConstructorHelpers.h"

APaint_Splash_MartinGameMode::APaint_Splash_MartinGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APaint_Splash_MartinHUD::StaticClass();
}

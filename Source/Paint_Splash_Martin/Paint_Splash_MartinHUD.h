// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Paint_Splash_MartinHUD.generated.h"

UCLASS()
class APaint_Splash_MartinHUD : public AHUD
{
	GENERATED_BODY()

public:
	APaint_Splash_MartinHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};


// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Paint_Splash_MartinGameMode.generated.h"

UCLASS(minimalapi)
class APaint_Splash_MartinGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APaint_Splash_MartinGameMode();
};



